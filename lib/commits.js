'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.fetchCommits = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var fetchCommits = exports.fetchCommits = function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(origin, options) {
    var log;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return (0, _utils.cmd)('git log --shortstat --pretty=format:' + LOG_FORMAT);

          case 2:
            log = _context.sent;
            return _context.abrupt('return', parseCommits(log, origin, options));

          case 4:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this);
  }));

  return function fetchCommits(_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();

var _utils = require('./utils');

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

var COMMIT_SEPARATOR = '__AUTO_CHANGELOG_COMMIT_SEPARATOR__';
var MESSAGE_SEPARATOR = '__AUTO_CHANGELOG_MESSAGE_SEPARATOR__';
var LOG_FORMAT = COMMIT_SEPARATOR + '%H%n%D%n%aI%n%an%n%ae%n%B' + MESSAGE_SEPARATOR;
var MATCH_COMMIT = /(.*)\n(.*)\n(.*)\n(.*)\n(.*)\n([\S\s]+)/;
var MATCH_STATS = /(\d+) files? changed(?:, (\d+) insertions?...)?(?:, (\d+) deletions?...)?/;
var TAG_PREFIX = 'tag: ';

// https://help.github.com/articles/closing-issues-via-commit-messages
var DEFAULT_FIX_PATTERN = /(?:close[sd]?|fixe?[sd]?|resolve[sd]?)\s(?:#(\d+)|(https?:\/\/.+?\/(?:issues|pull|pull-requests|merge_requests)\/(\d+)))/gi;

var MERGE_PATTERNS = [/Merge pull request #(\d+) from .+\n\n(.+)/, // Regular GitHub merge
/^(.+) \(#(\d+)\)(?:$|\n\n)/, // Github squash merge
/Merged in .+ \(pull request #(\d+)\)\n\n(.+)/, // BitBucket merge
/Merge branch .+ into .+\n\n(.+)[\S\s]+See merge request !(\d+)/, // GitLab merge
/^merg[ei].*/ig // Other merge
];

function parseCommits(string, origin) {
  var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

  var commits = string.split(COMMIT_SEPARATOR).slice(1).map(function (commit) {
    return parseCommit(commit, origin, options);
  });

  if (options.startingCommit) {
    var index = commits.findIndex(function (c) {
      return c.hash.indexOf(options.startingCommit) === 0;
    });
    if (index === -1) {
      throw new Error('Starting commit ' + options.startingCommit + ' was not found');
    }
    return commits.slice(0, index + 1);
  }

  return commits;
}

function parseCommit(commit, origin) {
  var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

  var _commit$match = commit.match(MATCH_COMMIT),
      _commit$match2 = _slicedToArray(_commit$match, 7),
      hash = _commit$match2[1],
      refs = _commit$match2[2],
      date = _commit$match2[3],
      author = _commit$match2[4],
      email = _commit$match2[5],
      tail = _commit$match2[6];

  var _tail$split = tail.split(MESSAGE_SEPARATOR),
      _tail$split2 = _slicedToArray(_tail$split, 2),
      message = _tail$split2[0],
      stats = _tail$split2[1];

  return _extends({
    hash: hash,
    shorthash: hash.slice(0, 7),
    author: author,
    email: email,
    date: date,
    tag: getTag(refs),
    subject: getSubject(message),
    message: message.trim(),
    fixes: getFixes(message, origin, options),
    merge: getMerge(message, origin),
    href: getCommitLink(hash, origin)
  }, getStats(stats.trim()));
}

function getTag(refs) {
  if (!refs) return null;
  var _iteratorNormalCompletion = true;
  var _didIteratorError = false;
  var _iteratorError = undefined;

  try {
    for (var _iterator = refs.split(', ')[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
      var ref = _step.value;

      if (ref.indexOf(TAG_PREFIX) === 0) {
        return ref.replace(TAG_PREFIX, '');
      }
    }
  } catch (err) {
    _didIteratorError = true;
    _iteratorError = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion && _iterator.return) {
        _iterator.return();
      }
    } finally {
      if (_didIteratorError) {
        throw _iteratorError;
      }
    }
  }

  return null;
}

function getSubject(message) {
  return message.match(/[^\n]+/)[0];
}

function getStats(stats) {
  if (!stats) return {};

  var _stats$match = stats.match(MATCH_STATS),
      _stats$match2 = _slicedToArray(_stats$match, 4),
      files = _stats$match2[1],
      insertions = _stats$match2[2],
      deletions = _stats$match2[3];

  return {
    files: parseInt(files || 0),
    insertions: parseInt(insertions || 0),
    deletions: parseInt(deletions || 0)
  };
}

function getFixes(message, origin) {
  var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

  var pattern = getFixPattern(options);
  var fixes = [];
  var match = pattern.exec(message);
  if (!match) return null;
  while (match) {
    var id = getFixID(match);
    var href = getIssueLink(match, id, origin, options.issueUrl);
    fixes.push({ id: id, href: href });
    match = pattern.exec(message);
  }
  return fixes;
}

function getFixID(match) {
  // Get the last non-falsey value in the match array
  for (var i = match.length; i >= 0; i--) {
    if (match[i]) {
      return match[i];
    }
  }
}

function getFixPattern(options) {
  if (options.issuePattern) {
    return new RegExp(options.issuePattern, 'g');
  }
  return DEFAULT_FIX_PATTERN;
}

function getMerge(message, origin, mergeUrl) {
  var _iteratorNormalCompletion2 = true;
  var _didIteratorError2 = false;
  var _iteratorError2 = undefined;

  try {
    for (var _iterator2 = MERGE_PATTERNS[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
      var pattern = _step2.value;

      var match = message.match(pattern);
      if (match) {
        var id = /^\d+$/.test(match[1]) ? match[1] : match[2];
        var _message = /^\d+$/.test(match[1]) ? match[2] : match[1];
        return {
          id: id,
          message: _message,
          href: getMergeLink(id, origin, mergeUrl)
        };
      }
    }
  } catch (err) {
    _didIteratorError2 = true;
    _iteratorError2 = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion2 && _iterator2.return) {
        _iterator2.return();
      }
    } finally {
      if (_didIteratorError2) {
        throw _iteratorError2;
      }
    }
  }

  return null;
}

function getCommitLink(hash, origin) {
  if (origin.hostname === 'bitbucket.org') {
    return origin.url + '/commits/' + hash;
  }
  return origin.url + '/commit/' + hash;
}

function getIssueLink(match, id, origin, issueUrl) {
  if ((0, _utils.isLink)(match[2])) {
    return match[2];
  }
  if (issueUrl) {
    return issueUrl.replace('{id}', id);
  }
  return origin.url + '/issues/' + id;
}

function getMergeLink(id, origin) {
  if (origin.hostname === 'bitbucket.org') {
    return origin.url + '/pull-requests/' + id;
  }
  if (origin.hostname === 'gitlab.com') {
    return origin.url + '/merge_requests/' + id;
  }
  return origin.url + '/pull/' + id;
}