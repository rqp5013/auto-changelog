'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _commander = require('commander');

var _fsExtra = require('fs-extra');

var _semver = require('semver');

var _semver2 = _interopRequireDefault(_semver);

var _package = require('../package.json');

var _origin = require('./origin');

var _commits = require('./commits');

var _releases = require('./releases');

var _template = require('./template');

var _utils = require('./utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

var DEFAULT_OPTIONS = {
  output: 'CHANGELOG.md',
  template: 'compact',
  remote: 'origin',
  commitLimit: 3
};

var NPM_VERSION_TAG_PREFIX = 'v';
var PACKAGE_OPTIONS_KEY = 'auto-changelog';

function getOptions(argv, pkg) {
  var options = new _commander.Command().option('-o, --output [file]', 'output file, default: ' + DEFAULT_OPTIONS.output).option('-t, --template [template]', 'specify template to use [compact, keepachangelog, json], default: ' + DEFAULT_OPTIONS.template).option('-r, --remote [remote]', 'specify git remote to use for links, default: ' + DEFAULT_OPTIONS.remote).option('-p, --package', 'use version from package.json as latest release').option('-v, --latest-version [version]', 'use specified version as latest release').option('-u, --unreleased', 'include section for unreleased changes').option('-l, --commit-limit [count]', 'number of commits to display per release, default: ' + DEFAULT_OPTIONS.commitLimit, _utils.parseLimit).option('-i, --issue-url [url]', 'override url for issues, use {id} for issue id').option('--issue-pattern [regex]', 'override regex pattern for issues in commit messages').option('--starting-commit [hash]', 'starting commit to use for changelog generation').version(_package.version).parse(argv);

  if (!pkg) {
    if (options.package) {
      throw new Error('package.json could not be found');
    }
    return _extends({}, DEFAULT_OPTIONS, options);
  }
  return _extends({}, DEFAULT_OPTIONS, pkg[PACKAGE_OPTIONS_KEY], options);
}

function getLatestVersion(options, pkg) {
  if (options.latestVersion) {
    if (!_semver2.default.valid(options.latestVersion)) {
      throw new Error('--latest-version must be a valid semver version');
    }
    return options.latestVersion;
  }
  if (options.package) {
    return NPM_VERSION_TAG_PREFIX + pkg.version;
  }
  return null;
}

exports.default = function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(argv) {
    var pkg, options, origin, commits, latestVersion, releases, log;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return (0, _fsExtra.pathExists)('package.json');

          case 2:
            _context.t0 = _context.sent;

            if (!_context.t0) {
              _context.next = 7;
              break;
            }

            _context.next = 6;
            return (0, _fsExtra.readJson)('package.json');

          case 6:
            _context.t0 = _context.sent;

          case 7:
            pkg = _context.t0;
            options = getOptions(argv, pkg);
            _context.next = 11;
            return (0, _origin.fetchOrigin)(options.remote);

          case 11:
            origin = _context.sent;
            _context.next = 14;
            return (0, _commits.fetchCommits)(origin, options);

          case 14:
            commits = _context.sent;
            latestVersion = getLatestVersion(options, pkg);
            releases = (0, _releases.parseReleases)(commits, origin, latestVersion, options);
            _context.next = 19;
            return (0, _template.compileTemplate)(options.template, { releases: releases });

          case 19:
            log = _context.sent;
            _context.next = 22;
            return (0, _fsExtra.writeFile)(options.output, log);

          case 22:
            return _context.abrupt('return', Buffer.byteLength(log, 'utf8') + ' bytes written to ' + options.output);

          case 23:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this);
  }));

  function run(_x) {
    return _ref.apply(this, arguments);
  }

  return run;
}();