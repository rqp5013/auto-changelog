'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.fetchOrigin = undefined;

var fetchOrigin = exports.fetchOrigin = function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(remote) {
    var originURL, origin, protocol, hostname;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return (0, _utils.cmd)('git config --get remote.' + remote + '.url');

          case 2:
            originURL = _context.sent;

            if (originURL) {
              _context.next = 5;
              break;
            }

            throw new Error('Git remote ' + remote + ' was not found');

          case 5:
            origin = (0, _parseGithubUrl2.default)(originURL);
            protocol = origin.protocol === 'http:' ? 'http:' : 'https:';
            hostname = origin.hostname || origin.host;
            return _context.abrupt('return', {
              hostname: hostname,
              url: protocol + '//' + hostname + '/' + origin.repo
            });

          case 9:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this);
  }));

  return function fetchOrigin(_x) {
    return _ref.apply(this, arguments);
  };
}();

var _parseGithubUrl = require('parse-github-url');

var _parseGithubUrl2 = _interopRequireDefault(_parseGithubUrl);

var _utils = require('./utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }