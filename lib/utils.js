'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.cmd = cmd;
exports.niceDate = niceDate;
exports.removeIndentation = removeIndentation;
exports.isLink = isLink;
exports.parseLimit = parseLimit;

var _child_process = require('child_process');

function _toArray(arr) { return Array.isArray(arr) ? arr : Array.from(arr); }

var MONTH_NAMES = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

// Simple util for calling a child process
function cmd(string) {
  var _string$split = string.split(' '),
      _string$split2 = _toArray(_string$split),
      cmd = _string$split2[0],
      args = _string$split2.slice(1);

  return new Promise(function (resolve, reject) {
    var child = (0, _child_process.spawn)(cmd, args);
    var data = '';

    child.stdout.on('data', function (buffer) {
      data += buffer.toString();
    });
    child.stdout.on('end', function () {
      return resolve(data);
    });
    child.on('error', reject);
  });
}

function niceDate(string) {
  var date = new Date(string);
  var day = date.getDate();
  var month = MONTH_NAMES[date.getMonth()];
  var year = date.getFullYear();
  return day + ' ' + month + ' ' + year;
}

function removeIndentation(string) {
  return string.replace(/\n +/g, '\n').replace(/^ +/, '');
}

function isLink(string) {
  return (/^http/.test(string)
  );
}

function parseLimit(limit) {
  return limit === 'false' ? false : parseInt(limit, 10);
}