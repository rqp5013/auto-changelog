'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

exports.parseReleases = parseReleases;

var _semver = require('semver');

var _semver2 = _interopRequireDefault(_semver);

var _utils = require('./utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MERGE_COMMIT_PATTERN = /^Merge (remote-tracking )?branch '.+'/;

function parseReleases(commits, origin, latestVersion, options) {
  var release = newRelease(latestVersion);
  var releases = [];
  var _iteratorNormalCompletion = true;
  var _didIteratorError = false;
  var _iteratorError = undefined;

  try {
    for (var _iterator = commits[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
      var commit = _step.value;

      if (commit.tag && _semver2.default.valid(commit.tag)) {
        if (release.tag || options.unreleased) {
          releases.push(_extends({}, release, {
            href: getCompareLink(commit.tag, release.tag || 'HEAD', origin),
            commits: release.commits.sort(sortCommits),
            major: commit.tag && release.tag && _semver2.default.diff(commit.tag, release.tag) === 'major'
          }));
        }
        release = newRelease(commit.tag, commit.date);
      }
      if (commit.merge) {
        release.merges.push(commit.merge);
      } else if (commit.fixes) {
        release.fixes.push({
          fixes: commit.fixes,
          commit: commit
        });
      } else if (filterCommit(commit, release, options.commitLimit)) {
        release.commits.push(commit);
      }
    }
  } catch (err) {
    _didIteratorError = true;
    _iteratorError = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion && _iterator.return) {
        _iterator.return();
      }
    } finally {
      if (_didIteratorError) {
        throw _iteratorError;
      }
    }
  }

  releases.push(release);
  return releases;
}

function newRelease() {
  var tag = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
  var date = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : new Date().toISOString();

  var release = {
    commits: [],
    fixes: [],
    merges: [],
    tag: tag,
    date: date,
    title: tag || 'Unreleased',
    niceDate: (0, _utils.niceDate)(date),
    isoDate: date.slice(0, 10)
  };
  return release;
}

function filterCommit(commit, release, limit) {
  if (_semver2.default.valid(commit.subject)) {
    // Filter out version commits
    return false;
  }
  if (MERGE_COMMIT_PATTERN.test(commit.subject)) {
    // Filter out merge commits
    return false;
  }
  if (release.merges.findIndex(function (m) {
    return m.message === commit.subject;
  }) !== -1) {
    // Filter out commits with the same message as an existing merge
    return false;
  }
  if (limit === false) {
    return true;
  }
  return release.commits.length < limit;
}

function getCompareLink(from, to, origin) {
  if (origin.hostname === 'bitbucket.org') {
    return origin.url + '/compare/' + to + '%0D' + from;
  }
  return origin.url + '/compare/' + from + '...' + to;
}

function sortCommits(a, b) {
  return b.insertions + b.deletions - (a.insertions + a.deletions);
}